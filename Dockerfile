FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=2.22.0

# install Prometheus
RUN curl -L https://github.com/prometheus/prometheus/releases/download/v${VERSION}/prometheus-${VERSION}.linux-amd64.tar.gz | tar zxvf - --strip-components 1

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
