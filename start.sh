#!/bin/bash

set -eu

mkdir -p /app/data/runtime /app/data/config

if [[ ! -f /app/data/config/prometheus.yml ]]; then
    echo "=> Creating config file on first run"
    sudo -u cloudron cp -n /app/code/prometheus.yml /app/data/config/prometheus.yml
fi

chown -R cloudron:cloudron /app/data

echo "=> Starting Prometheus"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/prometheus --config.file=/app/data/config/prometheus.yml --storage.tsdb.path=/app/data/runtime
